﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Microsoft.Samples.AzureKinectBasics
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Microsoft.Azure.Kinect.Sensor;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using Newtonsoft;

    using Microsoft.Azure.CognitiveServices.Vision.ComputerVision;
    using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;
    using Microsoft.Azure.CognitiveServices.Vision.CustomVision.Prediction;
    using Microsoft.Azure.CognitiveServices.Vision.CustomVision.Prediction.Models;

    using System.Collections.Generic;
    using Newtonsoft.Json.Linq;
    using System.Windows.Shapes;
   

    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        /// <summary>
        /// Azure Kinect sensor
        /// </summary>
        private readonly Device kinect = null;

        /// <summary>
        /// Azure Kinect transformation engine
        /// </summary>
        private readonly Transformation transform = null;

        /// <summary>
        /// Bitmap to display
        /// </summary>
        private readonly WriteableBitmap bitmap = null;

        /// <summary>
        /// Current status text to display
        /// </summary>
        private string statusText = null;

        /// <summary>
        /// The width in pixels of the color image from the Azure Kinect DK
        /// </summary>
        private readonly int colorWidth = 0;

        /// <summary>
        /// The height in pixels of the color image from the Azure Kinect DK
        /// </summary>
        private readonly int colorHeight = 0;

        /// <summary>
        /// Status of the application
        /// </summary>
        private bool running = true;

        /// <summary>
        /// Subscription key for Cognitive Services
        /// </summary>
        private const string subscriptionKey = "9bdf7ae4795f43faa8d0567cd5af7029";

        /// <summary>
        /// Connection to Azure Computer Vision Cognitive Service
        /// </summary>
        private ComputerVisionClient computerVision;

        /// <summary>
        /// Bounding box of the person
        /// </summary>
        BoundingBox boundingBox;

        /// <summary>
        /// List of features to find in images
        /// </summary>
        private static readonly List<VisualFeatureTypes> features =
            new List<VisualFeatureTypes>()
        {
            VisualFeatureTypes.Categories, VisualFeatureTypes.Description,
            VisualFeatureTypes.Faces, VisualFeatureTypes.ImageType,
            VisualFeatureTypes.Tags, VisualFeatureTypes.Objects
        };

        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            // Open the default device
            this.kinect = Device.Open();

            // Configure camera modes
            this.kinect.StartCameras(new DeviceConfiguration
            {
                ColorFormat = ImageFormat.ColorBGRA32,
                ColorResolution = ColorResolution.R1080p,
                DepthMode = DepthMode.NFOV_2x2Binned,
                SynchronizedImagesOnly = true
            });

            // Initialize the transformation engine
            this.transform = this.kinect.GetCalibration().CreateTransformation();

            this.colorWidth = this.kinect.GetCalibration().ColorCameraCalibration.ResolutionWidth;
            this.colorHeight = this.kinect.GetCalibration().ColorCameraCalibration.ResolutionHeight;

            // Create the computer vision client
            computerVision = new ComputerVisionClient(
                new ApiKeyServiceClientCredentials(subscriptionKey),
                new System.Net.Http.DelegatingHandler[] { })
                {
                    // You must use the same region as you used to get  
                    // your subscription keys. 
                    Endpoint = "https://centralus.api.cognitive.microsoft.com/"
            };

            this.bitmap = new WriteableBitmap(colorWidth, colorHeight, 96.0, 96.0, PixelFormats.Bgra32, null);

            this.DataContext = this;

            this.InitializeComponent();
        }

        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource ImageSource
        {
            get
            {
                return this.bitmap;
            }
        }

        /// <summary>
        /// Gets or sets the current status text to display
        /// </summary>
        public string StatusText
        {
            get
            {
                return this.statusText;
            }

            set
            {
                if (this.statusText != value)
                {
                    this.statusText = value;

                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }
                }
            }
        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            running = false;

            if (this.kinect != null)
            {
                this.kinect.Dispose();
            }
        }

        /// <summary>
        /// Handles the user clicking on the screenshot button
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void ScreenshotButton_Click(object sender, RoutedEventArgs e)
        {
            // Create a render target to which we'll render our composite image
            RenderTargetBitmap renderBitmap = new RenderTargetBitmap((int)CompositeImage.ActualWidth, (int)CompositeImage.ActualHeight, 96.0, 96.0, PixelFormats.Pbgra32);

            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext dc = dv.RenderOpen())
            {
                VisualBrush brush = new VisualBrush(CompositeImage);
                dc.DrawRectangle(brush, null, new System.Windows.Rect(new Point(), new Size(CompositeImage.ActualWidth, CompositeImage.ActualHeight)));
            }

            renderBitmap.Render(dv);

            BitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(renderBitmap));

            string time = System.DateTime.Now.ToString("hh'-'mm'-'ss", CultureInfo.CurrentUICulture.DateTimeFormat);

            string myPhotos = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);

            string path = System.IO.Path.Combine(myPhotos, "KinectScreenshot-" + time + ".png");

            // Write the new file to disk
            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Create))
                {
                    encoder.Save(fs);
                }

                this.StatusText = string.Format(Properties.Resources.SavedScreenshotStatusTextFormat, path);
            }
            catch (IOException)
            {
                this.StatusText = string.Format(Properties.Resources.FailedScreenshotStatusTextFormat, path);
            }
        }

        private Stream StreamFromBitmapSource(BitmapSource bitmap)
        {
            Stream jpeg = new MemoryStream();

            BitmapEncoder enc = new JpegBitmapEncoder();
            enc.Frames.Add(BitmapFrame.Create(bitmap));
            enc.Save(jpeg);
            jpeg.Position = 0;

            return jpeg;
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            int count = 0;

            while (running)
            {
                using (Image transformedDepth = new Image(ImageFormat.Depth16, colorWidth, colorHeight, colorWidth * sizeof(UInt16)))
                using (Capture capture = await Task.Run(() => { return this.kinect.GetCapture(); }))
                {
                    count++;

                    this.transform.DepthImageToColorCamera(capture, transformedDepth);

                    this.bitmap.Lock();

                    var color = capture.Color;
                    var region = new Int32Rect(0, 0, color.WidthPixels, color.HeightPixels);

                    unsafe
                    {
                        using (var pin = color.Memory.Pin())
                        {
                            this.bitmap.WritePixels(region, (IntPtr)pin.Pointer, (int)color.Size, color.StrideBytes);
                        }

                        if (boundingBox != null)
                        {
                            double y = (boundingBox.Top + boundingBox.Height / 2);
                            double x = (boundingBox.Left + boundingBox.Width / 2);

                            //this.StatusText = "The person is:" + transformedDepth.GetPixel<ushort>(y, x) + "mm away";
                        }
                    }

                    this.bitmap.AddDirtyRect(region);
                    this.bitmap.Unlock();

                    if (count % 30 == 0)
                    {
                        var stream = StreamFromBitmapSource(this.bitmap); // this returns jpeg image
                        _ = computerVision.AnalyzeImageInStreamAsync(stream, MainWindow.features).ContinueWith(async (Task<ImageAnalysis> analysis) =>
                        {
                        try
                        {
                            var client = new HttpClient();

                            // Request headers - replace this example key with your valid Prediction-Key.
                            client.DefaultRequestHeaders.Add("Prediction-Key", "579a6463ac204099ad4e1216ca951028");

                            // Prediction URL - replace this example URL with your valid Prediction URL.
                            string watchURL = "https://westus2.api.cognitive.microsoft.com/customvision/v3.0/Prediction/7a152c5c-91e8-4c40-b49f-f12324c1adf3/detect/iterations/Iteration4/image";
                            string crackURL = "https://westus2.api.cognitive.microsoft.com/customvision/v3.0/Prediction/7a152c5c-91e8-4c40-b49f-f12324c1adf3/detect/iterations/Iteration5nocolors/image";
                            HttpResponseMessage response;

                            byte[] data;
                            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                            encoder.Frames.Add(BitmapFrame.Create(this.bitmap));
                            using (MemoryStream ms = new MemoryStream())
                            {
                                encoder.Save(ms);
                                data = ms.ToArray();
                            }
                            Console.WriteLine("Summary:");
                            using (var content = new ByteArrayContent(data))
                            {
                                content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                                response = await client.PostAsync(crackURL, content);
                                var output = await response.Content.ReadAsStringAsync();
                                JObject json = JObject.Parse(output);
                                foreach (var item in json["predictions"])
                                {
                                    var probability = (int)item["probability"];
                                    if (probability >= 0.25)
                                    {

                                        double left = 1800 * (double)item["boundingBox"]["left"] + 90;
                                        double top = 1200 * (double)item["boundingBox"]["top"];
                                        double width = 1500 * (double)item["boundingBox"]["width"];
                                        double height = 1000 * (double)item["boundingBox"]["height"];

                                        BoundingBox rec = new BoundingBox();
                                        rec.Height = height - 50;
                                        rec.Width = width;
                                        rec.Left = left;
                                        rec.Top = top;
                                           
                                            this.boundingBox = rec;
                                            Console.WriteLine(" Tag: {0}, Probability {1}", item["tagName"], item["probability"]);
                                            Console.WriteLine("left: {0}, top: {1}, width:{2}, height:{3}", left, top ,width, height);
                                            System.Windows.Controls.Canvas.SetTop(boundingBoxRect, top);
                                            System.Windows.Controls.Canvas.SetLeft(boundingBoxRect, left);
                                            boundingBoxRect.Width = width;
                                            boundingBoxRect.Height = height;

                                            System.Windows.Controls.Canvas.SetTop(tagTextBlock, top-100);
                                            System.Windows.Controls.Canvas.SetLeft(tagTextBlock, left);
                                            double confidence = (double)item["probability"] * 100;
                                            tagTextBlock.Text = (string)item["tagName"] +" P:" + Math.Round(confidence, 2);
                                            System.Windows.Controls.TextBlock textBlockTag = new System.Windows.Controls.TextBlock();

                                            boundingCanvas.InvalidateVisual();
                                            boundingCanvas.UpdateLayout();
                                            boundingCanvas.Children.Add(tagTextBlock);
                                            boundingCanvas.Children.Add(boundingBoxRect);
                                            boundingCanvas.Children.Clear();

                                        }
                                        //else
                                        //{
                                        //    boundingCanvas.Children.Clear();
                                        //}
                                    }

                                }

                                //foreach (var caption in analysis.Result.Description.Captions)
                                //{
                                //    Console.WriteLine($"{caption.Text} with confidence {caption.Confidence}");
                                //}
                                //Console.WriteLine();

                            }
                            catch (System.Exception ex)
                            {
                                this.StatusText = ex.ToString();
                            }
                        }, TaskScheduler.FromCurrentSynchronizationContext());
                    }
                }
            }
        }
    }
}
